﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameOfLife_Test
{
    [TestClass]
    public class GameOfLifeTest
    {
        private GameOfLife.GameOfLife _gameOfLife;
        [TestInitialize]
        public void Ini()
        {
            _gameOfLife = new GameOfLife.GameOfLife();
        }

        [TestMethod]
        public void Board_ShouldExists_WhenGameIsCreated()
        {
            var board = _gameOfLife.Board;

            Assert.IsNotNull(board);
        }

        [TestMethod]
        public void Board_ShouldHas10x10Cells_WhenGameIsCreated()
        {
            var board = _gameOfLife.Board;

            Assert.AreEqual(100, board.Length);
            Assert.IsNotNull(board[9, 9]);
        }

        [TestMethod]
        [ExpectedException(typeof(System.IndexOutOfRangeException))]
        public void Board_ShouldNotHas11Rows_WhenGameIsCreated()
        {
            var board = _gameOfLife.Board;

            Assert.IsNull(board[10, 9]);
        }

        [TestMethod]
        [ExpectedException(typeof(System.IndexOutOfRangeException))]
        public void Board_ShouldNotHas11Columns_WhenGameIsCreated()
        {
            var board = _gameOfLife.Board;

            Assert.IsNull(board[9, 10]);
        }

        [TestMethod]
        public void Population_ShouldBeZero_WhenGameIsCreated()
        {
            Assert.AreEqual(0,_gameOfLife.Population);
        }

        [TestMethod]
        public void Board_ShouldAllCellsDead_WhenGameIsCreated()
        {
            var board = _gameOfLife.Board;

            for (var j = 0; j < 10; j += 1)
            {
                for (var k = 0; k < 10; k += 1)
                {
                    Assert.AreEqual(0, board[j, k]);
                }
            }
        }

        [TestMethod]
        public void Board_ShouldCellLive_WhenDoLifeOneCell()
        {
            _gameOfLife.DoLife(0, 0);
            var board =_gameOfLife.Board;

            Assert.AreEqual(1, board[0,0]);
        }

        [TestMethod]
        public void Population_ShouldBeOne_WhenDoLifeOneCell()
        {
            _gameOfLife.DoLife(0, 0);
            var population = _gameOfLife.Population;

            Assert.AreEqual(1, population);
        }

        [TestMethod]
        public void NextGeneration_ShouldNoAddCell_WhenBoardIsEmpty()
        {
            _gameOfLife.NextGeneration();
            var population = _gameOfLife.Population;

            Assert.AreEqual(0, population);
        }

        [TestMethod]
        public void NextGeneration_ShouldNoCell_WhenBoardOnlyOneCell()
        {
            _gameOfLife.DoLife(1,1);
            _gameOfLife.NextGeneration();
            var population = _gameOfLife.Population;

            Assert.AreEqual(0, population);
        }

        [TestMethod]
        public void NextGeneration_ShouldNoCell_WhenBoardHasTwoCelltogether()
        {
            _gameOfLife.DoLife(1, 1);
            _gameOfLife.DoLife(1, 2);
            _gameOfLife.NextGeneration();
            var population = _gameOfLife.Population;

            Assert.AreEqual(0, population);
        }

        [TestMethod]
        public void NextGeneration_ShouldFourCellInSquare_WhenBoardHasThreeCellsInL()
        {
            _gameOfLife.DoLife(1, 1);
            _gameOfLife.DoLife(1, 2);
            _gameOfLife.DoLife(2, 2);
            _gameOfLife.NextGeneration();
            var population = _gameOfLife.Population;
            var board = _gameOfLife.Board;

            Assert.AreEqual(4, population);
            Assert.AreEqual(1, board[1 ,1]);
            Assert.AreEqual(1, board[1, 2]);
            Assert.AreEqual(1, board[2, 2]);
            Assert.AreEqual(1, board[2, 1]);
        }

        [TestMethod]
        public void NextGeneration_ShouldFourCellInT_WhenBoardHasSevenCells()
        {
            _gameOfLife.DoLife(1, 1);
            _gameOfLife.DoLife(1, 2);
            _gameOfLife.DoLife(1, 3);
            _gameOfLife.DoLife(2, 2);
            _gameOfLife.NextGeneration();
            var population = _gameOfLife.Population;
            var board = _gameOfLife.Board;

            Assert.AreEqual(7, population);
            Assert.AreEqual(1, board[0, 2]);
            Assert.AreEqual(1, board[1, 1]);
            Assert.AreEqual(1, board[1, 2]);
            Assert.AreEqual(1, board[1, 3]);
            Assert.AreEqual(1, board[2, 1]);
            Assert.AreEqual(1, board[2, 2]);
            Assert.AreEqual(1, board[2, 3]);
        }

        [TestMethod]
        public void Generation_ShouldReturnCurrentGeneration_WhenCallNextGeneration()
        {
            Assert.AreEqual(0, _gameOfLife.Generation);
            _gameOfLife.NextGeneration();
            Assert.AreEqual(1, _gameOfLife.Generation);
            _gameOfLife.NextGeneration();
            Assert.AreEqual(2, _gameOfLife.Generation);
            _gameOfLife.NextGeneration();
        }
    }
}
