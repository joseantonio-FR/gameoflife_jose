﻿using System;
using System.Threading;

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inicialización");
            Console.WriteLine("1- Glider");
            Console.WriteLine("2- Small Explorer");
            Console.WriteLine("3- Tumbler");
            var key = Console.ReadLine();

            var gameOfLife = new GameOfLife();
            switch (key)
            {
                case "1":
                    GliderBoard(gameOfLife);
                    break;
                case "2":
                    SmallExplorerBoard(gameOfLife);
                    break;
                case "3":
                    TumblerBoard(gameOfLife);
                    break;
            }
            Console.Clear();

            while (true)
            {
                var board = gameOfLife.Board;

                for (var j = 0; j < 10; j += 1)
                {
                    var row = "";
                    for (var k = 0; k < 10; k += 1)
                    {
                        row += (board[j, k] == 1) ? "*" : "_";
                    }
                    Console.WriteLine(row);
                }
                Console.WriteLine("Generation:" + gameOfLife.Generation);
                Console.WriteLine("Population:" + gameOfLife.Population);
                gameOfLife.NextGeneration();
                //Console.ReadLine();
                Thread.Sleep(200);
                Console.Clear();
            }
            Console.ReadLine();
        }

        private static void TumblerBoard(GameOfLife gameOfLife)
        {
            gameOfLife.DoLife(2, 3);
            gameOfLife.DoLife(2, 4);
            gameOfLife.DoLife(2, 6);
            gameOfLife.DoLife(2, 7);
            gameOfLife.DoLife(3, 3);
            gameOfLife.DoLife(3, 4);
            gameOfLife.DoLife(3, 6);
            gameOfLife.DoLife(3, 7);
            gameOfLife.DoLife(4, 4);
            gameOfLife.DoLife(4, 6);
            gameOfLife.DoLife(5, 2);
            gameOfLife.DoLife(5, 4);
            gameOfLife.DoLife(5, 6);
            gameOfLife.DoLife(5, 8);
            gameOfLife.DoLife(6, 2);
            gameOfLife.DoLife(6, 4);
            gameOfLife.DoLife(6, 6);
            gameOfLife.DoLife(6, 8);
            gameOfLife.DoLife(7, 2);
            gameOfLife.DoLife(7, 3);
            gameOfLife.DoLife(7, 7);
            gameOfLife.DoLife(7, 8);
        }

        private static void SmallExplorerBoard(GameOfLife gameOfLife)
        {
            gameOfLife.DoLife(3, 4);
            gameOfLife.DoLife(4, 3);
            gameOfLife.DoLife(4, 4);
            gameOfLife.DoLife(4, 5);
            gameOfLife.DoLife(5, 3);
            gameOfLife.DoLife(5, 5);
            gameOfLife.DoLife(6, 4);
        }

        private static void GliderBoard(GameOfLife gameOfLife)
        {
            gameOfLife.DoLife(4, 4);
            gameOfLife.DoLife(4, 5);
            gameOfLife.DoLife(5, 5);
            gameOfLife.DoLife(5, 6);
            gameOfLife.DoLife(6, 5);
        }
    }
}
