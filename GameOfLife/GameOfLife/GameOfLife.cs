namespace GameOfLife
{
    public class GameOfLife
    {

        /*
         Las transiciones dependen del n�mero de c�lulas vecinas vivas:
        Una c�lula muerta con exactamente 3 c�lulas vecinas vivas "nace" (al turno siguiente estar� viva).
        Una c�lula viva con 2 � 3 c�lulas vecinas vivas sigue viva, en otro caso muere o permanece muerta (por "soledad" o "superpoblaci�n").
        */

        private int[,] _board;
        private const int _rows = 10;
        private const int _columns = 10;
        private const int LIVE = 1;
        private const int DEAD = 0; 
        private int _generation;

        public GameOfLife()
        {
            _board = new int[_rows, _columns];
        }

        public int[,] Board {
            get
            {
                return _board;
            }
        }

        public int Generation
        {
            get { return _generation; }
        }

        public int Population
        {
            get
            {
                var population = 0;
                foreach (var value in _board)
                {
                    population += value;
                }
                return population;
            }
        }

        public void DoLife(int x, int y)
        {
            _board[x, y] = LIVE;
        }

        public void NextGeneration()
        {
            var nextBoard = new int[_rows, _columns];
            for (var rowIndex = 0; rowIndex < _rows; rowIndex += 1)
            {
                nextBoard = UpdateRowNextGeneration(rowIndex, nextBoard);
            }
            _board = nextBoard;
            _generation++;
        }

        private int[,] UpdateRowNextGeneration(int rowIndex, int[,] nextBoard)
        {
            for (var colIndex = 0; colIndex < _columns; colIndex += 1)
            {
                var neighbourLived = GetNeighbourLived(rowIndex, colIndex);
                if (neighbourLived == 3) nextBoard[rowIndex, colIndex] = LIVE;
                var status = _board[rowIndex, colIndex];
                if (status == DEAD) continue;
                if (neighbourLived == 2) nextBoard[rowIndex, colIndex] = LIVE;
            }
            return nextBoard;
        }

        private int GetNeighbourLived(int rowIndex , int colIndex)
        {
            var neighbours = 0;
            neighbours += GetNeighbourValue(rowIndex - 1, colIndex - 1);
            neighbours += GetNeighbourValue(rowIndex - 1, colIndex);
            neighbours += GetNeighbourValue(rowIndex - 1, colIndex + 1);
            neighbours += GetNeighbourValue(rowIndex, colIndex - 1);
            neighbours += GetNeighbourValue(rowIndex, colIndex + 1);
            neighbours += GetNeighbourValue(rowIndex + 1, colIndex - 1);
            neighbours += GetNeighbourValue(rowIndex + 1, colIndex);
            neighbours += GetNeighbourValue(rowIndex + 1, colIndex + 1);
            return neighbours;
        }

        private int GetNeighbourValue(int rowIndex, int colIndex)
        {
            if (rowIndex < 0) return 0;
            if (rowIndex >= _rows) return 0;
            if (colIndex < 0) return 0;
            if (colIndex >= _columns) return 0;
            return _board[rowIndex, colIndex];
        }
    }
}